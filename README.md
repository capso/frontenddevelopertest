# CapSo frontend developer test

## Setup

Install all required Node modules
```bash
npm install  
```

Install all required Bower components
```bash
bower install  
```

Update webdriver manager
```bash
./node_modules/grunt-protractor-runner/scripts/webdriver-manager-update  
```

## Running the project and testing

To start the project run
```bash
grunt serve:beta  
```

To run unit tests suites use
```bash
grunt test
```

To start end-to-end Protractor test run
```bash
grunt e2e
```