// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2015-07-20 using
// generator-karma 0.8.3

module.exports = function(config) {
  'use strict';

  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // base path, that will be used to resolve files and exclude
    basePath: '.',

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
        'bower_components/jquery/dist/jquery.js',
        'bower_components/angular/angular.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-aria/angular-aria.js',
        'bower_components/angular-messages/angular-messages.js',
        'bower_components/angular-cookies/angular-cookies.js',
        'bower_components/angular-resource/angular-resource.js',
        'bower_components/angular-route/angular-route.js',
        'bower_components/angular-sanitize/angular-sanitize.js',
        'bower_components/angular-touch/angular-touch.js',
        'bower_components/angular-material/angular-material.js',
        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        'bower_components/moment/moment.js',
        'bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
        'bower_components/angular-google-chart/ng-google-chart.js',
        'bower_components/fullcalendar/dist/fullcalendar.js',
        'bower_components/angular-ui-calendar/src/calendar.js',
        'bower_components/angular-ui-router/release/angular-ui-router.js',
        'bower_components/autosize/dist/autosize.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/bootstrap-select/dist/js/bootstrap-select.js',
        'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        'bower_components/jquery.bootgrid/dist/jquery.bootgrid.js',
        'bower_components/jquery.videoBG/jquery.videoBG.js',
        'bower_components/ng-img-crop/compile/minified/ng-img-crop.js',
        'bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js',
        'bower_components/numeral/numeral.js',
        'bower_components/sweetalert/dist/sweetalert.min.js',
        'bower_components/jquery-ui/ui/jquery-ui.js',
        'bower_components/angular-ui-slider/src/slider.js',
        'bower_components/angular-filter/dist/angular-filter.min.js',
        'bower_components/angular-smart-table/dist/smart-table.js',
        'bower_components/bootstrap-daterangepicker/daterangepicker.js',
        'bower_components/angular-daterangepicker/js/angular-daterangepicker.js',
        'bower_components/SHA-1/sha1.js',
        'bower_components/angulartics/src/angulartics.js',
        'bower_components/angulartics-google-tag-manager/lib/angulartics-google-tag-manager.js',
        'app/**/*.module.js',
        'app/**/*.js'
    ],
    exclude: [
        'app/**/*.e2e.js',
        'app/**/*.e2e-page.js'
    ],

    // web server port
    port: 8080,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      'PhantomJS'
    ],

    /*
    hides JSHint
    reporters: ['spec'],
    */
    specReporter: {
        maxLogLines: 5,         // limit number of lines logged per test
        suppressErrorSummary: true,  // do not print error summary
        suppressFailed: false,  // do not print information about failed tests
        suppressPassed: false,  // do not print information about passed tests
        suppressSkipped: true,  // do not print information about skipped tests
        showSpecTiming: false // print the time elapsed for each spec
    },

    plugins: [
      'karma-phantomjs-launcher',
      'karma-ng-html2js-preprocessor',
      'karma-jasmine',
      'karma-spec-reporter'
    ],
    preprocessors: { 
      '**/*.html': ['ng-html2js'] 
    }, 
    ngHtml2JsPreprocessor: { 
      stripPrefix: 'app', 
      moduleName: 'karmaGeneratedAngularTemplates' 
    },

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
