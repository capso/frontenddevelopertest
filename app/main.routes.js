(function() {
'use strict';



angular
.module('capsoTestApp')
.config(AppRoutesConfig);



AppRoutesConfig.$inject=['$stateProvider','$urlRouterProvider'];
function AppRoutesConfig( $stateProvider,  $urlRouterProvider ) {
    
    $urlRouterProvider.otherwise('test-overview');

    $stateProvider
        .state('test-overview', {
            url: '/test-overview',
            templateUrl: 'pages/test-overview/test-overview.view.html',
            controller: 'TestOverviewCtrl',
            controllerAs: 'TestOverviewCtrl'
        });

}



})();

