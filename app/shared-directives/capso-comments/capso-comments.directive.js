(function() {
'use strict';


angular
.module('capsoTestApp')
.directive('capsoComments', function() {
    return {
        scope: {
            newsItem: '='
        },
        templateUrl: '/scripts/directives/capso-comments/capso-comments.template.html',
        bindToController: true,
        controllerAs: 'CapsoCommentsCtrl',
        controller: CapsoCommentsCtrl
    };
});


CapsoCommentsCtrl.$inject=['UserAccountService','StateNavigationService','CommentsService'];
function CapsoCommentsCtrl( UserAccountService,  StateNavigationService,  CommentsService ) {

    var me = this;
    me.newCommentText = '';

    me.goToPersonState = StateNavigationService.goToPersonState.bind(StateNavigationService);

    UserAccountService.get().then(function(loggedInUserAccount) {
        me.loggedInUserId = loggedInUserAccount.id;
        me.loggedInUserProfileImgUrl = loggedInUserAccount.profileImage;
    });

    CommentsService.getNewsItemComments(me.newsItem.id).then(function(comments) {
        me.comments = comments;
    });

    me.addComment = function() {

        var newCommentText = me.newCommentText;
        me.newCommentText = '';

        me.isSavingComment = true;
        CommentsService.addCommentToNewsItem(me.newsItem.id, newCommentText).then(function(newComment) {
            me.comments.push(newComment);
        })
        .catch(function() {
            me.isSavingCommentError = true;
            me.newCommentText = newCommentText;
        })
        .finally(function() { me.isSavingComment = false; });
    };
}


})();