'use strict';



var serveTasks = [
  'clean:server',
  'html2js:main',
  'jshint',
  'wiredep',
  'concurrent:local',
  'autoprefixer',
  'connect:livereload',
  'watch'
];


module.exports = {
  'test': {
    description: '',
    tasks: [
      'clean:server',
      'concurrent:local',
      'autoprefixer',
      'connect:test',
      'karma',
      'e2e',
    ]
  },
  'e2e': {
    description: 'Run Protractor e2e testing',
    tasks: [
      'protractor_webdriver:local',
      'protractor:singlerun'
    ]
  },
  'serve:beta': serveTasks

};

