'use strict';

module.exports = function (grunt, data) {
  return {
    options: {
      sourceMap: true
    },
    dist: {
      files: {
        '<%= rootPath %>/.tmp/main.css': data.appPath + '/main.scss'
      }
    } 
  };
};