'use strict';

module.exports = function (grunt, data) {
  return {
    options: {
      jshintrc: '.jshintrc',
      reporter: require('jshint-stylish'),
      verbose:true,
    },
    all: {
      src: [
        'Gruntfile.js',
        //'<%= yeoman.appPath %>/scripts/{,*/}*.js'
        data.appPath + '/scripts/**/*.js'
      ]
    },
    test: {
      src: [data.appPath + '/scripts/**/*.spec.js']
    }
  };
};