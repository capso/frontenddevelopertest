'use strict';

module.exports = function (grunt, data) {
  return {
    app: {
      src: [data.appPath + '/index.html'],
      ignorePath:  /\.\.\//
    },
    sass: {
      src: [data.appPath + '/{,*/}*.{scss,sass}'],
      ignorePath: data.rootPath + '/bower_components/'
    }
  };
};