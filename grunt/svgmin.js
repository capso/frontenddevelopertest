'use strict';

module.exports = function (grunt, data) {
  return {
    dist: {
      files: [{
        expand: true,
        cwd: data.appPath + '/images',
        src: '{,*/}*.svg',
        dest: data.distPath + '/images'
      }]
    }
  };
};