'use strict';

module.exports = function (grunt, data) { 
  return {
    dist : {
      src: [data.appPath + '/scripts/**/*.js', 'README.md'],
      options: {
        destination: 'doc',
        configure : 'jsdoc.conf.json'
      }
    }
  };
};