'use strict';

module.exports = function (grunt, data) {
  return {
    dist: {
      files: [{
        expand: true,
        cwd: data.rootPath + '/.tmp/concat/scripts',
        src: ['*.js', '!oldieshim.js'],
        dest: data.rootPath + '/.tmp/concat/scripts'
      }]
    }
  };
};